#/bin/bash

LOCALDIR=`cd "$( dirname $0 )" && pwd`
cd $LOCALDIR
source ./bin.sh

rm -rf ./boot
mkdir ./boot

if [ -e ./boot.img ];then
  cp -frp ./boot.img $bin/AIK/
  cd $bin/AIK
  ./unpackimg.sh ./boot.img
  mv ./ramdisk ../../boot/
  mv ./split_img ../../boot/
  cd $LOCALDIR
  chmod 777 -R ./boot
else
  echo "没有boot.img，请重试"
fi
